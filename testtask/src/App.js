import { navMenu, banner, content, footerText } from "./textContent";
import "./App.css";
import Banner from "./components/banner";
import Header from "./components/header";
import Content from "./components/content";
import Users from "./components/users";
import Footer from "./components/footer";

function App() {
  return (
    <>
      <Header links={navMenu} />
      <Banner
        heading={banner.heading}
        text={banner.body}
        buttonName={banner.buttonName}
      />
      <Content
        heading={content.heading}
        subheading={content.subheading}
        text={content.body}
        buttonName={content.buttonName}
      />
      <Users />
      <Footer text={footerText} />
    </>
  );
}

export default App;
