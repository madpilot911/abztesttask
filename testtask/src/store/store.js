import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";

const initialState = {
    users: [],
};

export const store = createStore(
    reducer,
    composeWithDevTools(applyMiddleware(thunk))
);

function reducer(state = initialState, action) {
    switch (action.type) {
        case "LOAD_USERS":
            return {...state, users: [...state.users, ...action.payload] };
        default:
            return state;
    }
}

const loadUsers = (payload) => ({ type: "LOAD_USERS", payload });

export const fetchUsers = (dispatch) =>
    fetch(
        "https://frontend-test-assignment-api.abz.agency/api/v1/users?page=1&count=9"
    )
    .then(function(response) {
        return response.json();
    })
    .then(function(data) {
        dispatch(loadUsers(data.users));
    });