const navMenu = [{
        name: "About me",
        href: "href1",
    },
    {
        name: "Relationships",
        href: "href2",
    },
    {
        name: "Requirements",
        href: "href3",
    },
    {
        name: "Users",
        href: "href4",
    },
    {
        name: "Sign Up",
        href: "href5",
    },
];

const banner = {
    heading: "Test assignment for front-end developers",
    body: "Front-end developers make sure the user sees and interacts with all the necessary elements to ensure conversion. Therefore, responsive design, programming languages and specific frameworks are the must-have skillsets to look for when assessing your front-end developers.",
    buttonName: "Sign up",
};

const content = {
    heading: "Let's get acquainted",
    subheading: "I'm a good front-end developer",
    body: "What defines a good front-end developer is one that has skilled knowledge of HTML, CSS, JS with a vast understanding of User design thinking as they'll be building web interfaces with accessibility in mind. They should also be excited to learn, as the world of Front-End Development keeps evolving.",
    buttonName: "Sign up",
};

const footerText = `\u00A9 abz.agency specially for the test task`;

export { navMenu, banner, content, footerText };