import styles from "./header.module.scss";
import logo from "../image/Logo.svg";

export default function Header(props) {
  const { links } = props;
  return (
    <header className={styles.header}>
      <div className={styles.wrapper}>
        <img src={logo} alt="logo" />
        <nav className={styles.nav}>
          {links.map((link, index) => (
            <a key={index} href={link.href} className={styles.link}>
              {link.name}
            </a>
          ))}
        </nav>
      </div>
    </header>
  );
}
