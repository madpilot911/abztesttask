import styles from "./banner.module.scss";
import Button from "./button";

export default function Banner(props) {
  const { heading, text, buttonName, buttonHandler } = props;
  return (
    <section className={styles.banner}>
      <div className={styles.mainWrapper}>
        <div className={styles.contentWrapper}>
          <h1 className={styles.heading}>{heading}</h1>
          <p className={styles.text}>{text}</p>
          <Button title={buttonName} onClick={buttonHandler} />
        </div>
      </div>
    </section>
  );
}
