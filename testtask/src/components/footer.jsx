import styles from "./footer.module.scss";

export default function Footer(props) {
  const { text } = props;
  return (
    <footer className={styles.footer}>
      <p className={styles.text}>{text}</p>
    </footer>
  );
}
