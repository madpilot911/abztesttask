import styles from "./content.module.scss";
import imageLarge from "../image/Image-387x340.svg";
import imageMedium from "../image/Image-328x287.svg";
import imageSmall from "../image/Image-296x260.svg";
import Button from "./button";

export default function Content(props) {
  const { heading, subheading, text, buttonName } = props;
  return (
    <section className={styles.content}>
      <div className={styles.mainWrapper}>
        <picture>
          <source media="(max-width:360px)" srcset={imageMedium} />
          <source media="(max-width:768px)" srcset={imageSmall} />
          <img src={imageLarge} alt="" />
        </picture>
        <div className={styles.contentWrapper}>
          <h2 className={styles.heading}>{heading}</h2>
          <h3 className={styles.subheading}>{subheading}</h3>
          <p className={styles.text}>{text}</p>
          <Button title={buttonName} />
        </div>
      </div>
    </section>
  );
}
