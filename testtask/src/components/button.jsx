import styles from "./button.module.scss";

export default function Button(props) {
  const { title, disabled } = props;
  return (
    <button className={styles.btn} disabled={disabled}>
      {title}
    </button>
  );
}
