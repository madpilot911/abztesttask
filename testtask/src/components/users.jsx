import { useEffect } from "react";
import styles from "./users.module.scss";
import { store, fetchUsers } from "../store/store";
// import { useDispatch } from "react-redux";

function User(props) {
  const { photo, name, position, email, phone } = props;
  return (
    <div className={styles.card}>
      <img src={photo} alt="" className={styles.avatar} />
      <h4 className={styles.name}>{name}</h4>
      <p className={styles.position}>{position}</p>
      <a className={styles.email} href={`mailto: ${email}`}>
        {email}
      </a>
      <a className={styles.phone} href={`tel: ${phone}`}>
        {phone}
      </a>
    </div>
  );
}

export default function UsersList() {
  const users = store.getState().users;
  useEffect(() => fetchUsers(), []);
  return (
    <section className={styles.section}>
      <div className={styles.innerWrapper}>
        {users.map((user, index) => (
          <User
            key={user.id}
            photo={user.photo}
            name={user.name}
            position={user.position}
            email={user.email}
            phone={user.phone}
          />
        ))}
      </div>
    </section>
  );
}
